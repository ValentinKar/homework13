<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки


function isAction() 
{ 
		return $_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET['id']) && !empty($_GET['action']);
}; 

function isEdit()   // Изменить мероприятие
{ 
		return $_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET['id']) && $_GET['action'] === 'edit';
}; 

function isDescriptNew()   // Зафиксировать изменение мероприятия
{ 
	return $_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['descript_new']) && !empty($_POST['id']);  
}; 

function isNewOccasions()    // Новое мероприятие 
{ 
	return $_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['description']); 
}; 
<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require_once('function.php'); 

$host = 'localhost';    // localhost 
$user = 'karpayev';
$password = 'neto1052'; 
$database = 'karpayev'; 
$dbport = 3306; 

$pdo = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password, [
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
	]);

if ( isAction() && $_GET['action'] === 'delete' ) {     // удалить мероприятие
  $id = (integer)$_GET['id'];  
  $statement = $pdo->prepare("DELETE FROM tasks WHERE id=?;");  
  $statement->execute( ["{$id}"] );  
  header('Location: index.php'); 
}; 

if ( isAction() && $_GET['action'] === 'done' ) {      // выполнить мероприятие
  $id = (integer)$_GET['id'];  
  $statement = $pdo->prepare("UPDATE tasks SET is_done = 2 WHERE id = ?;"); 
  $statement->execute( ["{$id}"] );  
  header('Location: index.php'); 
}; 

if ( isDescriptNew() ) {       // изменить мероприятие
  $id = (integer)$_POST['id'];  
  $description = (string)$_POST['descript_new'];  
  $statement = $pdo->prepare("UPDATE tasks SET description = ? WHERE id = ?;"); 
  $statement->execute( ["{$description}", "{$id}"] );  
  header('Location: index.php'); 
}; 

if ( isNewOccasions() ) {       // новое мероприятие
  $description = (string)$_POST['description'];  
  //$time = time(); 
  $statement = $pdo->prepare( "INSERT INTO tasks (description, is_done, date_added)
  VALUES ( ?, 1, now() );" ); 
  $statement->execute( ["{$description}"] );  

    $statement = $pdo->query( "SELECT LAST_INSERT_ID();"); 
    foreach ($statement as $number) { 
    echo "Последний уникальный id = " . $number['LAST_INSERT_ID()'] . "<br />"; 
    } 
}; 

?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="UTF-8">
<title>tasks</title>
<style>
body {
 font-family: sans-serif;
 font-size: 15px;
}
table {
border: 1px solid black; 
padding: 4px;
}
td {
border: 1px solid black; 
padding: 4px;
}
</style>
</head>
<body>

<h1>Список мероприятий</h1>

<form method="POST">
    <input type="text" name="description" placeholder="Описание задачи" value="" />
    <input type="submit" name="save" value="Добавить мероприятие" />
</form>

<br /><br />

<table>
    <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
    </tr>
<tr>

<?php  
$statement = $pdo->prepare("SELECT * FROM tasks;");  // prepare - подготовить запрос
$statement->execute();            // execute - выполнять запрос

foreach ( $statement as $row )  :  ?> 
	<tr>
		<td>
      <?php  
        if ( isEdit() && $_GET['id'] === $row['id'] ) {       
        ?>

        <form method="POST">
            <input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
            <input type="text" name="descript_new" value="<?php echo $row['description']; ?>" />
            <input type="submit" name="edit_descript" value="Изменить" />
        </form>

    <?php  
        } 
        else {
          echo htmlspecialchars($row['description']);  
        }; 
      ?>
    </td>
		<td><?php  echo $row['date_added'];  ?></td>
		<td><?php    
            switch( $row['is_done'] )  
              {
              case 1: 
                echo 'не выполнено'; 
                break;
              case 2: 
                echo 'выполнено'; 
                break; 
              default: 
                echo 'странный статус'; 
                break; 
              };
      ?>
    </td>

    <td>
        <a href='?id=<?php echo $row['id']; ?>&action=edit'>Изменить</a> 
        <a href='?id=<?php echo $row['id']; ?>&action=done'>Выполнить</a> 
        <a href='?id=<?php echo $row['id']; ?>&action=delete'>Удалить</a> 
    </td>

	</tr>
<?php  endforeach;  ?>

</table>
</body>
</html>